import pipeline.pieces.*;

public class Main {

    private Runnable buildEngine() {
        // very simple - assumes one source, does not try to detect human errors (cycles/meaningless connections).
        // if/when we want more building-blocks of type source, we'll build a pipeline to manage their lifecycle.
        StdinSource source = new StdinSource();
        Filter filter = new Filter(x -> x > 0);
        FixedEventWindow windowOf2 = new FixedEventWindow(2);
        FoldSum foldsum = new FoldSum();
        FixedEventWindow windowOf3 = new FixedEventWindow(3);
        FoldMedian foldmedian = new FoldMedian();
        StdoutSink stdoutSink = new StdoutSink();

        source.setConsumer(filter);
        filter.setConsumer(windowOf2);
        windowOf2.setConsumer(foldsum);
        foldsum.setConsumer(windowOf3);
        windowOf3.setConsumer(foldmedian);
        foldmedian.setConsumer(stdoutSink);
        return source;
    }

    public static void main(String[] args) throws InterruptedException {
        Runnable engine = new Main().buildEngine();
        engine.run();
    }

}
