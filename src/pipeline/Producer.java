package pipeline;

/**
 * A producer component. The API is for a producer to call the consumer's {@link Consumer#receiveInput(Object)} method with the produced item.
 * @param <T>
 */
public interface Producer<T> extends NamedComponent {

    void setConsumer(Consumer<T> output);

}
