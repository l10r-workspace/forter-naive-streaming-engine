package pipeline;

/**
 * A consumer component: the API is for consumers to be called with an input event whenever one is available.
 * The consumer is then responsible for handling the input and discarding or passing it forward.
 *
 * @param <T>
 */
public interface Consumer<T> extends NamedComponent {

    void receiveInput(T event);

}
