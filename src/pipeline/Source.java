package pipeline;

/**
 * An event source is Producer which is Runnable so it can be run directly of in a thread.
 * @param <T>
 */
public interface Source<T> extends Runnable, Producer<T> {
}
