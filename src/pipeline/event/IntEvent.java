package pipeline.event;

import pipeline.Event;

/**
 * An event bearing an Integer payload
 */
public class IntEvent implements Event<Integer> {

    private final Integer payload;

    public IntEvent(Integer num) {
        this.payload = num;
    }

    @Override
    public Integer getPayload() {
        return this.payload;
    }
}
