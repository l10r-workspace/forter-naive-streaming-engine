package pipeline.event;

import pipeline.Event;

/**
 * An event bearing an array of integers as payload
 */
public class IntArrayEvent implements Event<Integer[]> {

    private final Integer[] payload;

    public IntArrayEvent(Integer... nums) {
        this.payload = nums;
    }

    /**
     * @return a copy of the payload as Events are meant to be immutable
     */
    @Override
    public Integer[] getPayload() {
        return this.payload.clone();
    }
}
