package pipeline;

/**
 * An Event in the system; events pass through the engine
 * @param <T>
 */
public interface Event<T> {

    T getPayload();

}
