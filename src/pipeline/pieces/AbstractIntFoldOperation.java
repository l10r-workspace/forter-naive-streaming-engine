package pipeline.pieces;

import pipeline.Consumer;
import pipeline.Pipe;
import pipeline.event.IntArrayEvent;
import pipeline.event.IntEvent;

/**
 * Abstract class for folding an IntArray Event into an Integer Event
 */
public abstract class AbstractIntFoldOperation implements Pipe<IntArrayEvent, IntEvent> {

    private Consumer<IntEvent> output = null;

    /**
     * To be implemented in subclasses
     * @param event an IntArrayEvent
     * @return an Integer, the result of applying the operator to event.
     */
    abstract protected Integer fold(IntArrayEvent event);

    @Override
    public void setConsumer(Consumer<IntEvent> output) {
        this.output = output;
    }

    @Override
    public void receiveInput(IntArrayEvent event) {
        IntEvent newEvent = new IntEvent(fold(event));
        if (output != null) {
            output.receiveInput(newEvent);
        }
    }

}
