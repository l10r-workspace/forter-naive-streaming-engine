package pipeline.pieces;

import pipeline.Consumer;
import pipeline.Pipe;
import pipeline.event.IntArrayEvent;
import pipeline.event.IntEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * A fixed-width Integer buffer: buffers IntEvents and produces an IntArrayEvent once the buffer is filled.
 */
public class FixedEventWindow implements Pipe<IntEvent, IntArrayEvent> {

    private final int windowSize;
    private List<Integer> buffer;
    private Consumer<IntArrayEvent> output = null;

    public FixedEventWindow(int windowSize) {
        if (windowSize < 1) {
            throw new IllegalArgumentException("windowSize must be positive");
        }
        this.windowSize = windowSize;
        this.buffer = getFreshBuffer();
    }

    private ArrayList<Integer> getFreshBuffer() {
        return new ArrayList<>(windowSize);
    }

    @Override
    public void setConsumer(Consumer<IntArrayEvent> output) {
        this.output = output;
    }

    @Override
    public void receiveInput(IntEvent event) {
        buffer.add(event.getPayload());
        if (buffer.size() == windowSize) {
            IntArrayEvent newEvent = new IntArrayEvent(buffer.toArray(new Integer[]{}));
            if (output != null) {
                output.receiveInput(newEvent);
            }
            buffer = getFreshBuffer();
        }
    }
}
