package pipeline.pieces;

import pipeline.event.IntArrayEvent;

import java.util.Arrays;

/**
 * A median fold operation; note that the result is always rounded up to int.
 */
public class FoldMedian extends AbstractIntFoldOperation {

    @Override
    protected Integer fold(IntArrayEvent event) {
        Integer[] arr = event.getPayload();
        // TODO there's a more efficient way to do this without sorting first
        Arrays.sort(arr);
        int result;
        int len = arr.length / 2;
        if (len % 2 == 1) {
            result = arr[len];
        } else {
            result = (arr[len/2-1] + arr[len/2]) / 2;
        }
        return result;
    }

}
