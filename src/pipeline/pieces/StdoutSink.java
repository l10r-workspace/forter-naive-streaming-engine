package pipeline.pieces;

import pipeline.Consumer;
import pipeline.Pipe;
import pipeline.event.IntEvent;

public class StdoutSink implements Pipe<IntEvent, IntEvent> {

    private Consumer<IntEvent> output = null;

    public StdoutSink() {
        // blank
    }

    @Override
    public void setConsumer(Consumer<IntEvent> output) {
        this.output = output;
    }

    @Override
    public void receiveInput(IntEvent event) {
        Util.say(event.getPayload().toString());
        if (output != null) {
            output.receiveInput(event);
        }
    }
}
