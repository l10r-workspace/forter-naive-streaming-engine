package pipeline.pieces;

import pipeline.Consumer;
import pipeline.Source;
import pipeline.event.IntEvent;

import java.util.Scanner;

/**
 * An Event Source based on Standard input
 */
public class StdinSource implements Source<IntEvent> {

    private Consumer<IntEvent> output = null;

    public StdinSource() {
        // blank
    }

    @Override
    public void setConsumer(Consumer<IntEvent> output) {
        this.output = output;
    }

    @Override
    public void run() {
        if (this.output == null) {
            throw new IllegalStateException("Output must be initialized for this source to start running.");
        }
        Scanner scanner = new Scanner(System.in);
        Util.say("Started - please provide integers (EOF exits)");
        while (scanner.hasNext()) {
            int num = scanner.nextInt();
            Util.say("> " + num);
            this.output.receiveInput(new IntEvent(num));
        }
        Util.say("Finished - EOF");
    }
}
