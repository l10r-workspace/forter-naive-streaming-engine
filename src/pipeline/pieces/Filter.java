package pipeline.pieces;

import pipeline.Consumer;
import pipeline.Pipe;
import pipeline.event.IntEvent;

import java.util.function.IntPredicate;

/**
 * An Integer filter; provided a predicate filters the IntEvents
 */
public class Filter implements Pipe<IntEvent, IntEvent> {

    private final IntPredicate predicate;

    private Consumer<IntEvent> output = null;

    public Filter(IntPredicate predicate) {
        this.predicate = predicate;
    }

    @Override
    public void setConsumer(Consumer<IntEvent> output) {
        this.output = output;
    }

    @Override
    public void receiveInput(IntEvent event) {
        if (output != null && predicate.test(event.getPayload())) {
            output.receiveInput(event);
        }
    }
}
