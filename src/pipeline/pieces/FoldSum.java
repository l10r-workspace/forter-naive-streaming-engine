package pipeline.pieces;

import pipeline.event.IntArrayEvent;

import java.util.Arrays;

/**
 * A Sum fold operation: sums the Integers in the provided events
 */
public class FoldSum extends AbstractIntFoldOperation {

    @Override
    protected Integer fold(IntArrayEvent event) {
        return Arrays.stream(event.getPayload()).reduce(0, Integer::sum);
    }

}
