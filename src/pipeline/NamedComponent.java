package pipeline;

/**
 * TODO
 */
public interface NamedComponent {

    default String getName() { return this.getClass().getSimpleName(); };

}
