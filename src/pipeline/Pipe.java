package pipeline;

/**
 * A Pipe is a part of the streaming pipeline which is both consuming and producing items
 * @param <C> Consumer Type
 * @param <P> Producer Type
 */
public interface Pipe<C, P> extends Consumer<C>, Producer<P> {
}
